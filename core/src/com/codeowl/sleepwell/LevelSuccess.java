package com.codeowl.sleepwell;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

/**
 * Created by smaniotov on 03/05/2018.
 */

public class LevelSuccess implements Screen {
    private final SleepWell game;
    private final Game previousLevel;


    public LevelSuccess(SleepWell game, Game previousLevel) {
        this.game = game;
        this.previousLevel = previousLevel;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

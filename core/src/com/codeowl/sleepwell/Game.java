package com.codeowl.sleepwell;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;

public class Game implements Screen {
    private final SleepWell game;

    private final int platformWidth = 300;
    private final int platformHeight = 120;

    private int pointCount = 0;
    private int lifes;

    private int screenWidth;
    private int screenHeight;

    private Music gameMusic;

    private Sound endLevelSound;
    private Sound pointSound;
    private Sound hurtSound;

    private Texture platformImage;
    private Rectangle platform;
    private Texture heartImage;

    private Rectangle heart;
    private Array<Texture> obstacleImages;
    private Array<Texture> pointImages;
    private Rectangle obstacle;
    private Array<Rectangle> obstacles;
    private Rectangle point;
    private Array<Rectangle> points;

    private long lastSpawn;

    public Game (final SleepWell game) {
        this.game = game;
        lifes = 3;

        screenHeight = Gdx.graphics.getHeight();
        screenWidth = Gdx.graphics.getWidth();

        game.setScreen(new SplashScreen(game));

        gameMusic = Gdx.audio.newMusic(Gdx.files.internal("game/in_game_music.mp3"));
        pointSound = Gdx.audio.newSound(Gdx.files.internal("game/point_sound.wav"));
        hurtSound = Gdx.audio.newSound(Gdx.files.internal("game/hurt_sound.mp3"));
        endLevelSound = Gdx.audio.newSound(Gdx.files.internal("game/level_end_sound.mp3"));

        gameMusic.setLooping(true);
        gameMusic.play();

        heartImage = new Texture(Gdx.files.internal("game/heart/heart_medium.png"));

        heart = new Rectangle();
        heart.width = 70;
        heart.height = 70;

        platformImage = new Texture(Gdx.files.internal("game/platform.png"));

        platform = new Rectangle();
        platform.x = screenWidth / 2 - platformWidth / 2;
        platform.y = screenHeight / 8;
        platform.width = platformWidth;
        platform.height = platformHeight;

        obstacleImages = new Array<Texture>(4);

        obstacleImages.add(new Texture(Gdx.files.internal("game/obstacle/obstacle_1.png")));
        obstacleImages.add(new Texture(Gdx.files.internal("game/obstacle/obstacle_2.png")));
        obstacleImages.add(new Texture(Gdx.files.internal("game/obstacle/obstacle_3.png")));
        obstacleImages.add(new Texture(Gdx.files.internal("game/obstacle/obstacle_4.png")));

        pointImages = new Array<Texture>(4);

        pointImages.add(new Texture(Gdx.files.internal("game/point/points_1.png")));
        pointImages.add(new Texture(Gdx.files.internal("game/point/points_2.png")));
        pointImages.add(new Texture(Gdx.files.internal("game/point/points_3.png")));
        pointImages.add(new Texture(Gdx.files.internal("game/point/points_4.png")));

        obstacles = new Array<Rectangle>();
        points = new Array<Rectangle>();

        spawn();
    }

    private void spawn () {
        if(MathUtils.random(1) == 0) {
            spawnObstacles();
        } else {
            spawnPoints();
        }
    }

    private void spawnObstacles () {
        obstacle = new Rectangle();
        obstacle.width = 150;
        obstacle.height = 150;
        obstacle.x = MathUtils.random(0, screenWidth - obstacle.width);
        obstacle.y = screenHeight;
        obstacles.add(obstacle);
        lastSpawn = TimeUtils.nanoTime();
    }

    private void spawnPoints () {
        point = new Rectangle();
        point.width = 150;
        point.height = 150;
        point.x = MathUtils.random(0, screenWidth - point.width);
        point.y = screenHeight;
        points.add(point);
        lastSpawn = TimeUtils.nanoTime();
    }


    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        game.batch.begin();
        game.batch.draw(platformImage, platform.x, platform.y, platform.width, platform.height);

        for (Rectangle p: points) {
            game.batch.draw(pointImages.get(0), p.x, p.y, p.width, p.height);
        }

        for (Rectangle o: obstacles) {
            game.batch.draw(obstacleImages.get(0), o.x, o.y, o.width, o.height);
        }
        
        for(int i =0; i < lifes; i++) {
            game.batch.draw(heartImage, screenWidth - (heart.width * i ) - 40 - (heart.width / 2), screenHeight - 50 - (heart.height / 2), heart.width, heart.height);
        }

        game.font.draw(game.batch, String.valueOf(pointCount), 20, screenHeight - 20);
        game.batch.end();

        if (Gdx.input.isTouched()) {
            Vector3 touchPos = new Vector3();
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            platform.x = touchPos.x - (platform.width / 2);
        }

        if (pointCount >= 1000) {
            game.setScreen(new LevelSuccess(game, this));
            endLevelSound.play();
            dispose();
        }

        if (lifes == 0) {
            game.setScreen(new LevelFailure(game, this));
            dispose();
        }


        if (platform.x < 0) platform.x = 0;
        if (platform.x > screenWidth) platform.x = screenWidth - platform.width / 2;

        if (TimeUtils.nanoTime() - lastSpawn > 2140000000) spawn();

        Iterator<Rectangle> pointIter = points.iterator();
        Iterator<Rectangle> obstacleIter = obstacles.iterator();

        while(pointIter.hasNext()) {
            Rectangle point = pointIter.next();
            point.y -= 200 * Gdx.graphics.getDeltaTime();
            if(point.y + 64 < 0) pointIter.remove();
            if (point.overlaps(platform)) {
                pointIter.remove();
                pointCount += 100;
                pointSound.play();
            }
        }

        while(obstacleIter.hasNext()) {
            Rectangle obstacle = obstacleIter.next();
            obstacle.y -= 200 * Gdx.graphics.getDeltaTime();
            if(obstacle.y + 64 < 0) obstacleIter.remove();
            if(obstacle.overlaps(platform)) {
                obstacleIter.remove();
                hurtSound.play();
                lifes--;
            }
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        heartImage.dispose();
        platformImage.dispose();
        gameMusic.dispose();
        hurtSound.dispose();
        endLevelSound.dispose();
        pointSound.dispose();

    }
}

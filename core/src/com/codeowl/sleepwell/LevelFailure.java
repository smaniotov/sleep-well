package com.codeowl.sleepwell;

import com.badlogic.gdx.Screen;

/**
 * Created by smaniotov on 03/05/2018.
 */

public class LevelFailure implements Screen {
    private final Game previousLevel;
    private final SleepWell game;

    public LevelFailure(SleepWell game, Game previousLevel) {
        this.previousLevel = previousLevel;
        this.game = game;
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

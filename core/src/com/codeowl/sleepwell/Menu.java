package com.codeowl.sleepwell;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public class Menu implements Screen {
    private final SleepWell game;
    private SpriteBatch batch;
    private Texture backgroundImage;
    private Texture gameLogoImage;

    private int screenHeight;
    private int screenWidth;

    public Menu (final SleepWell game) {
        this.game = game;
        this.batch = game.batch;

        gameLogoImage = new Texture(Gdx.files.internal("menu/game_logo.png"));
        backgroundImage = new Texture(Gdx.files.internal("menu/background.png"));
        game.font.getData().setScale(4);
        game.font.setColor(Color.BLACK);
        this.screenHeight = Gdx.graphics.getHeight();
        this.screenWidth = Gdx.graphics.getWidth();
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();

        batch.draw(gameLogoImage, screenWidth / 3, screenHeight / 2);
        game.font.draw(batch, "Touch anywhere to start!", (screenWidth / 2 - game.font.getRegion().getRegionWidth()), screenHeight / 3);

        batch.end();

        if (Gdx.input.isTouched()) {
            game.setScreen(new Game(game));
            dispose();
        }
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    public void dispose() {
        backgroundImage.dispose();
    }

}

package com.codeowl.sleepwell;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

public class SplashScreen implements Screen {
    private SpriteBatch batch;
    private final SleepWell game;

    private Texture logoImage;
    private Texture uniclusterImage;
    private Texture unipImage;
    private Texture currentSplashScreen;
    private Array<Texture> splashScreens;

    private long startTime;

    private int imageSizeIncrease;


    public SplashScreen(final SleepWell game) {
        this.batch = game.batch;
        this.game = game;

        logoImage = new Texture(Gdx.files.internal("splash/logo.png"));
        uniclusterImage = new Texture(Gdx.files.internal("splash/unicluster.png"));
        unipImage = new Texture(Gdx.files.internal("splash/unip.png"));

        splashScreens = new Array<Texture>(3);

        splashScreens.add(uniclusterImage);
        splashScreens.add(unipImage);
        splashScreens.add(logoImage);


        startTime = TimeUtils.millis();

        this.splashScreenChange();
    }

    private void splashScreenChange() {
        long time = TimeUtils.millis() - startTime;

        if (time >= 0 && time < 2000) {
            currentSplashScreen = splashScreens.get(0);
        } else if (time >= 2000 && time < 4000) {
            currentSplashScreen = splashScreens.get(1);
        } else if (time >= 4000 && time < 6000) {
            currentSplashScreen = splashScreens.get(2);
        } else {
            game.setScreen(new Menu(game));
            dispose();
        }
    }

    public void render(float delta) {
        Gdx.gl.glClearColor(1, 1, 1, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        splashScreenChange();

        imageSizeIncrease = (int) ((TimeUtils.millis() - startTime) % 2000) / 15;

        batch.begin();

        batch.draw(currentSplashScreen,
                Gdx.graphics.getWidth() / 2 - ((currentSplashScreen.getWidth() + imageSizeIncrease) / 2),
                Gdx.graphics.getHeight() / 2 - ((currentSplashScreen.getHeight() + imageSizeIncrease) / 2),
                currentSplashScreen.getWidth() + imageSizeIncrease,
                currentSplashScreen.getHeight() + imageSizeIncrease);

        batch.end();

    }

    public void dispose() {
        logoImage.dispose();
        uniclusterImage.dispose();
        unipImage.dispose();
    }

    @Override
    public void show() {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }
}

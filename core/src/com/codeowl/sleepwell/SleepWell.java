package com.codeowl.sleepwell;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class SleepWell extends Game {
    protected SpriteBatch batch;
    protected BitmapFont font;

    public void create() {
        batch = new SpriteBatch();
        font = new BitmapFont();
        this.setScreen(new SplashScreen(this));
    }

    public void render () {
        super.render();
    }

    public void dispose () {
        batch.dispose();
    }

}
